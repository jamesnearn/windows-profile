#https://support.microsoft.com/en-us/help/314834/how-to-clear-the-windows-paging-file-at-shutdown

# Set-ItemProperty : Requested registry access is not allowed.
# At C:\_tools\windows-profile\Registry\ClearPageFileAtShutdown.ps1:4 char:1
# + Set-ItemProperty . ClearPageFileAtShutdown "1"
# + ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     + CategoryInfo          : PermissionDenied: (HKEY_LOCAL_MACH...mory Management:String) [Set-ItemProperty], SecurityException
#     + FullyQualifiedErrorId : System.Security.SecurityException,Microsoft.PowerShell.Commands.SetItemPropertyCommand


Push-Location
#Set-Location HKCU:\Software\hsg
Set-Location "HKLM:\SYSTEM\CurrentControlSet\Control\Session Manager\Memory Management"
Set-ItemProperty . ClearPageFileAtShutdown "1"
Pop-Location